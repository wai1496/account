<?php if($__is_woo_enabled): ?>
	<?php if(auth()->user()->can('superadmin') || auth()->user()->can('woocommerce.syc_categories') || auth()->user()->can('woocommerce.sync_products') || auth()->user()->can('woocommerce.sync_orders') || auth()->user()->can('woocommerce.map_tax_rates') || auth()->user()->can('woocommerce.access_woocommerce_api_settings')): ?>
		<li class="bg-woocommerce treeview <?php echo e(in_array($request->segment(1), ['woocommerce']) ? 'active active-sub' : '', false); ?>">
		    <a href="#">
		        <i class="fa fa-wordpress"></i>
		        <span class="title"><?php echo app('translator')->getFromJson('woocommerce::lang.woocommerce'); ?></span>
		        <span class="pull-right-container">
		            <i class="fa fa-angle-left pull-right"></i>
		        </span>
		    </a>

		    <ul class="treeview-menu">
		    	<li class="<?php echo e($request->segment(1) == 'woocommerce' && empty($request->segment(2)) ? 'active active-sub' : '', false); ?>">
					<a href="<?php echo e(action('\Modules\Woocommerce\Http\Controllers\WoocommerceController@index'), false); ?>">
						<i class="fa fa-refresh"></i>
						<span class="title">
							<?php echo app('translator')->getFromJson('woocommerce::lang.sync'); ?>
						</span>
				  	</a>
				</li>
				<li class="<?php echo e($request->segment(1) == 'woocommerce' && $request->segment(2) == 'view-sync-log' ? 'active active-sub' : '', false); ?>">
					<a href="<?php echo e(action('\Modules\Woocommerce\Http\Controllers\WoocommerceController@viewSyncLog'), false); ?>">
						<i class="fa fa-history"></i>
						<span class="title">
							<?php echo app('translator')->getFromJson('woocommerce::lang.sync_log'); ?>
						</span>
				  	</a>
				</li>
				<?php if(auth()->user()->can('woocommerce.access_woocommerce_api_settings') ): ?>
				<li class="<?php echo e($request->segment(1) == 'woocommerce' && $request->segment(2) == 'api-settings' ? 'active active-sub' : '', false); ?>">
					<a href="<?php echo e(action('\Modules\Woocommerce\Http\Controllers\WoocommerceController@apiSettings'), false); ?>">
						<i class="fa fa-cogs"></i>
						<span class="title">
							<?php echo app('translator')->getFromJson('woocommerce::lang.api_settings'); ?>
						</span>
				  	</a>
				</li>
				<?php endif; ?>
	        </ul>
		</li>
	<?php endif; ?>
<?php endif; ?>